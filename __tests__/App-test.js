/**
 * @format
 */

import 'kony-ui';
import kony from 'kony';
import App from '../App';

// Note: test renderer must be required after kony-ui.
import renderer from 'kony-test-renderer';

it('renders correctly', () => {
  renderer.create(<App />);
});
