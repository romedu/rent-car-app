import kony from "kony";
import { View } from "kony-ui";
import { uiRouter, BackButton, Switch, Route } from "kony-router-ui";
import LandingScreen from "./src/screens/LandingScreen";
import AppScreen from "./src/screens/AppScreen";
import { AuthContextProvider } from "./src/context/AuthContext";
import generalStyles from "./src/generalStyles";

const App = () => (
	<uiRouter>
		<BackButton>
			<AuthContextProvider>
				<View style={generalStyles.container}>
					<Switch>
						<Route exact path="/" component={LandingScreen} />
						<Route path="/" component={AppScreen} />
					</Switch>
				</View>
			</AuthContextProvider>
		</BackButton>
	</uiRouter>
);

export default App;
