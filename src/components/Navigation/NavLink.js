import kony from "kony";
import { StyleSheet } from "kony-ui";
import { Link, withRouter } from "kony-router-ui";
import Icon from "kony-ui-vector-icons/Feather";

const NavLink = ({ linkTo, iconName, location }) => (
	<Link to={linkTo} style={styles.navItem} underlayColor="#f0f4f7">
		<Icon
			name={iconName}
			size={22}
			color={linkTo === location.pathname ? "blue" : "black"}
		/>
	</Link>
);

const styles = StyleSheet.create({
	navItem: {
		alignItems: "center"
	}
});

export default withRouter(NavLink);
