import kony, { Fragment } from "kony";
import { StyleSheet, View } from "kony-ui";
import NavLink from "./NavLink";
import TextButton from "../UI/TextButton";
import Icon from "kony-ui-vector-icons/Feather";

const Nav = ({ userId, logoutHandler }) => {
	// The login/logout links are displayed based on the current userId state
	return (
		<View style={styles.nav}>
			<NavLink linkTo="/vehicles" iconName="shopping-cart" />
			<NavLink linkTo="/rents" iconName="key" />
			<NavLink linkTo="/inspections" iconName="check" />
			{!userId ? (
				<NavLink linkTo="/auth/login" iconName="users" />
			) : (
				<Fragment>
					<NavLink linkTo="/my-rents" iconName="star" />
					<NavLink linkTo="/reports" iconName="book-open" />
				</Fragment>
			)}
			{userId && (
				<TextButton onPressHandler={logoutHandler}>
					<Icon name="log-out" size={22} color="black" />
				</TextButton>
			)}
		</View>
	);
};

const styles = StyleSheet.create({
	nav: {
		flexDirection: "row",
		justifyContent: "space-around",
		borderTopColor: "darkgray",
		borderTopWidth: 0.3,
		paddingTop: 10,
		paddingBottom: 10
	}
});

export default Nav;
