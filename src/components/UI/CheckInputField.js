import kony from "kony";
import { View, Text, StyleSheet, CheckBox } from "kony-ui";

const CheckInputField = props => {
	const { label, name, value, onChangeHandler } = props;

	return (
		<View style={styles.checkBoxField}>
			<CheckBox
				type="checkbox"
				value={value}
				onValueChange={newValue => {
					onChangeHandler(newValue, name, "checkbox");
				}}
			/>
			<Text> {label} </Text>
		</View>
	);
};

const styles = StyleSheet.create({
	checkBoxField: {
		flexDirection: "row"
	}
});

export default CheckInputField;
