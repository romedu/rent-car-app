import kony from "kony";
import { Text, StyleSheet } from "kony-ui";

const TextHeader = ({
	fontSize = 20,
	textAlign = "left",
	color = "black",
	children
}) => (
	<Text style={[styles, { color, fontSize, textAlign }]}> {children} </Text>
);

const styles = StyleSheet.create({
	fontWeight: "bold"
});

export default TextHeader;
