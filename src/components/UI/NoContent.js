import kony from "kony";
import { View } from "kony-ui";
import TextHeader from "./TextHeader";

const NoContent = () => (
	<View>
		<TextHeader fontSize={15}> No items where found... </TextHeader>
	</View>
);

export default NoContent;
