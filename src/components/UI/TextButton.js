import kony from "kony";
import { Text, StyleSheet, TouchableWithoutFeedback } from "kony-ui";

const TextButton = ({ children, onPressHandler }) => (
	<TouchableWithoutFeedback onPress={onPressHandler} style={styles.textButton}>
		<Text>{children}</Text>
	</TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
	textButton: {
		alignItems: "center"
	}
});

export default TextButton;
