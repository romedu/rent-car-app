import kony from "kony";
import { View, Text } from "kony-ui";
import { fromCamelToCapital } from "../../utils";

// Accepts an object of properties
const PropertiesList = ({ properties }) => {
	const propertyEntries = Object.entries(properties),
		content = propertyEntries.map(([propName, propValue]) => (
			<View>
				<Text> {`• ${fromCamelToCapital(propName)}: ${propValue}`} </Text>
			</View>
		));

	return <View>{content}</View>;
};

export default PropertiesList;
