import kony from "kony";
import { View, TextInput } from "kony-ui";
import TextHeader from "./TextHeader";

const TextInputField = props => {
	const {
		label,
		name,
		placeholder,
		value,
		isPassword,
		keyboardType,
		onChangeHandler
	} = props;

	return (
		<View>
			<View>
				<TextHeader fontSize={14}>{label}</TextHeader>
			</View>
			<View>
				<TextInput
					value={value}
					placeholder={placeholder}
					placeholderTextColor="lightgray"
					onChangeText={newValue => onChangeHandler(newValue, name)}
					secureTextEntry={isPassword}
					keyboardType={keyboardType}
					underlineColorAndroid="lightgray"
				/>
			</View>
		</View>
	);
};

// const styles = StyleSheet.create({
// 	input: {
// 		borderBottomWidth: 1,
// 		borderBottomColor: "lightgray"
// 	}
// });

export default TextInputField;
