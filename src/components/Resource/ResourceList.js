import kony from "kony";
import { FlatList } from "kony-ui";
import ResourceThumbnail from "./ResourceThumbnail";
import NoContent from "../UI/NoContent";

const ResourceList = ({ match, resources, getResources, rootRoute }) => {
	const getMoreResources = () => getResources(true);

	return (
		<FlatList
			data={resources}
			onEndReachedThreshold={0.7}
			onEndReached={getMoreResources}
			ListEmptyComponent={NoContent}
			renderItem={({ item }) => (
				<ResourceThumbnail match={match} rootRoute={rootRoute} {...item} />
			)}
		/>
	);
};

export default ResourceList;
