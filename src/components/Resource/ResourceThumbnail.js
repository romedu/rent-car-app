import kony from "kony";
import { View, Image, StyleSheet } from "kony-ui";
import { Link } from "kony-router-ui";
import TextHeader from "../UI/TextHeader";
import PropertiesList from "../UI/PropertiesList";

// Update the link pathname
const ResourceThumbnail = ({
	id,
	make,
	model,
	match,
	rootRoute,
	builtYear,
	frontImage,
	...extraProps
}) => (
	<Link to={`${rootRoute || match.path}/${id}`} underlayColor="#f0f4f7">
		<View style={styles.thumbnail}>
			<View>
				<Image source={{ uri: frontImage }} style={styles.image} />
			</View>
			<View style={styles.thumbnailContent}>
				<View>
					<TextHeader fontSize={14}>
						{`${make} ${model} ${builtYear}`}
					</TextHeader>
				</View>
				<PropertiesList properties={extraProps} />
			</View>
		</View>
	</Link>
);

const styles = StyleSheet.create({
	thumbnail: {
		flexDirection: "row",
		borderColor: "lightgray",
		borderWidth: 0.5
	},
	thumbnailContent: {
		flex: 1,
		paddingLeft: 15,
		flexDirection: "column",
		justifyContent: "center"
	},
	image: {
		width: 120,
		height: 120
	}
});

export default ResourceThumbnail;
