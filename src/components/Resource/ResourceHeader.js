import kony from "kony";
import { View, Image, StyleSheet } from "kony-ui";
import TextHeader from "../UI/TextHeader";

const ResourceHeader = ({ title, imgUrl }) => (
	<View>
		<View>
			<Image source={{ uri: imgUrl }} style={styles.image} />
		</View>
		<TextHeader>{title}</TextHeader>
	</View>
);

const styles = StyleSheet.create({
	image: {
		width: "100%",
		height: 250
	}
});

export default ResourceHeader;
