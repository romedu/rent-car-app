import kony, { useState, useEffect, useContext } from "kony";
import {
	View,
	Modal,
	Image,
	Button,
	Alert,
	ScrollView,
	ActivityIndicator
} from "kony-ui";
import TextHeader from "../UI/TextHeader";
import PropertiesList from "../UI/PropertiesList";
import { AuthContext } from "../../context/AuthContext";
import generalStyles from "../../generalStyles";

const InspectionModal = ({
	showingModal,
	subTitle,
	frontImage,
	closeHandler,
	rentId,
	employeeId
}) => {
	const { token } = useContext(AuthContext),
		[isLoading, setLoader] = useState(false),
		[inspectionState, setInspection] = useState({
			rentId,
			employeeId,
			zest: !!Math.round(Math.random()),
			fuelAmount: Math.random().toFixed(1)
		}),
		[isInspected, setInspectedState] = useState(false),
		createInspection = async () => {
			setLoader(true);

			try {
				const fetchOptions = {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"Authorization": token
						},
						body: JSON.stringify(inspectionState)
					},
					fetchResult = await fetch(
						"https://car-rent-api.herokuapp.com/api/inspections",
						fetchOptions
					),
					{ error } = await fetchResult.json();

				if (error) throw new Error(error.message);
				setInspectedState(true);
			} catch (error) {
				setLoader(false);
				Alert.alert("Error", error.message);
			}
		};

	useEffect(() => {
		const returnRent = async () => {
			try {
				const fetchOptions = {
						method: "PATCH",
						headers: {
							"Content-Type": "application/json",
							"Authorization": token
						}
					},
					fetchResult = await fetch(
						`https://car-rent-api.herokuapp.com/api/rents/${rentId}`,
						fetchOptions
					),
					{ error } = await fetchResult.json();

				if (error) throw new Error(error.message);
				closeHandler({ returnRent: true });
			} catch (error) {
				setLoader(false);
				Alert.alert("Error", error.message);
			}
		};

		if (isInspected) returnRent();
	}, [isInspected]);

	return (
		<Modal
			visible={showingModal}
			animationType="slide"
			style={generalStyles.container}
		>
			<ScrollView style={generalStyles.container}>
				<TextHeader> Vehicle Inspection </TextHeader>
				<View>
					<Image
						style={generalStyles.modalImage}
						source={{ uri: frontImage }}
					/>
				</View>
				<TextHeader fontSize={16}>{subTitle}</TextHeader>
				<PropertiesList properties={inspectionState} />
				<Button title="Confirm" onPress={createInspection} />
				<Button title="Cancel" onPress={closeHandler} />
				{isLoading && <ActivityIndicator size="small" />}
			</ScrollView>
		</Modal>
	);
};

export default InspectionModal;
