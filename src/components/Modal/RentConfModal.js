import kony, { useState, useContext } from "kony";
import {
	View,
	Modal,
	Image,
	Button,
	Alert,
	ScrollView,
	ActivityIndicator
} from "kony-ui";
import TextHeader from "../UI/TextHeader";
import PropertiesList from "../UI/PropertiesList";
import TextInputField from "../UI/TextInputField";
import { AuthContext } from "../../context/AuthContext";
import useInputHandler from "../../hooks/useInputHandler";
import generalStyles from "../../generalStyles";

const RentConfModal = ({
	make,
	model,
	builtYear,
	frontImage,
	showingModal,
	closeHandler,
	vehicleData
}) => {
	const { token } = useContext(AuthContext),
		[isLoading, setLoader] = useState(false),
		[rentState, rentChangeHandler] = useInputHandler({ rentDays: 1 }),
		createRent = async () => {
			setLoader(true);

			try {
				const newRentData = {
						vehicleId: vehicleData.id,
						rentDays: rentState.rentDays,
						employeeId: Math.ceil(Math.random() * 3) // Randomly gets an employee
					},
					fetchOptions = {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							"Authorization": token
						},
						body: JSON.stringify(newRentData)
					},
					fetchResult = await fetch(
						"https://car-rent-api.herokuapp.com/api/rents",
						fetchOptions
					),
					{ error } = await fetchResult.json();

				if (error) throw new Error(error.message);
				closeHandler({ makeUnavailable: true }); // Passing true as a param makes the vehicle unavailable for rent
			} catch (error) {
				setLoader(false);
				Alert.alert("Error", error.message);
			}
		};

	return (
		<Modal
			visible={showingModal}
			animationType="slide"
			style={generalStyles.container}
		>
			<ScrollView style={generalStyles.container}>
				<TextHeader> Confirm Rent </TextHeader>
				<View>
					<Image
						style={generalStyles.modalImage}
						source={{ uri: frontImage }}
					/>
				</View>
				<TextHeader fontSize={16}>
					{`${make} ${model} ${builtYear}`}
				</TextHeader>
				<TextInputField
					label="Rent Days:"
					name="rentDays"
					value={rentState.rentDays}
					keyboardType="number-pad"
					onChangeHandler={rentChangeHandler}
				/>
				<PropertiesList properties={vehicleData} />
				<Button title="Confirm" onPress={createRent} />
				<Button title="Cancel" onPress={closeHandler} />
				{isLoading && <ActivityIndicator size="small" />}
			</ScrollView>
		</Modal>
	);
};

export default RentConfModal;
