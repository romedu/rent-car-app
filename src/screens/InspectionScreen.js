import kony, { useEffect, Fragment } from "kony";
import { ScrollView, Alert, ActivityIndicator } from "kony-ui";
import ResourceHeader from "../components/Resource/ResourceHeader";
import PropertiesList from "../components/UI/PropertiesList";
import TextHeader from "../components/UI/TextHeader";
import useFetch from "../hooks/useFetch";
import generalStyles from "../generalStyles";

const InspectionScreen = ({ match }) => {
	const { inspectionId } = match.params,
		[isLoading, fetchError, inspectionState] = useFetch(
			`https://car-rent-api.herokuapp.com/api/inspections/${inspectionId}`
		);

	useEffect(() => {
		if (fetchError) Alert.alert("Error", fetchError.message);
	}, [fetchError]);

	let content;

	if (inspectionState) {
		const {
				id,
				make,
				model,
				builtYear,
				frontImage,
				...inspectionData
			} = inspectionState.inspection,
			screenTitle = `Inspection #${id}`,
			screenSubTitle = `${make} ${model} ${builtYear}`;

		content = (
			<Fragment>
				<ResourceHeader title={screenTitle} imgUrl={frontImage} />
				<TextHeader fontSize={16}> {screenSubTitle} </TextHeader>
				<PropertiesList properties={inspectionData} />
			</Fragment>
		);
	}

	return (
		<ScrollView style={generalStyles.container}>
			{isLoading ? <ActivityIndicator /> : content}
		</ScrollView>
	);
};

export default InspectionScreen;
