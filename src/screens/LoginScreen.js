import kony from "kony";
import { View, Text, Button, Alert } from "kony-ui";
import { Link } from "kony-router-ui";
import TextHeader from "../components/UI/TextHeader";
import TextInputField from "../components/UI/TextInputField";
import useInputHandler from "../hooks/useInputHandler";
import generalStyles from "../generalStyles";

const LoginScreen = ({ loginHandler }) => {
	const [inputState, inputChangeHandler] = useInputHandler({
			username: "",
			password: ""
		}),
		submitHandler = async () => {
			try {
				const fetchOptions = {
						method: "POST",
						headers: { "Content-Type": "application/json" },
						body: JSON.stringify(inputState)
					},
					fetchResult = await fetch(
						"https://car-rent-api.herokuapp.com/api/auth/login",
						fetchOptions
					),
					{ error, token } = await fetchResult.json();

				if (error) throw new Error(error.message);
				else loginHandler(token);
			} catch (error) {
				Alert.alert("Error", error.message);
			}
		};

	return (
		<View style={generalStyles.container}>
			<View>
				<TextHeader> Login </TextHeader>
			</View>
			<View>
				<TextInputField
					name="username"
					placeholder="Username"
					label="Username"
					value={inputState.username}
					onChangeHandler={inputChangeHandler}
				/>
				<TextInputField
					name="password"
					placeholder="Password"
					label="Password"
					value={inputState.password}
					onChangeHandler={inputChangeHandler}
					isPassword
				/>
				<View>
					<Link to="/auth/register">
						<Text> New user? </Text>
					</Link>
				</View>
				<Button title="Submit" onPress={submitHandler} />
			</View>
		</View>
	);
};

export default LoginScreen;
