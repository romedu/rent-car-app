import kony, { useState, useEffect, Fragment } from "kony";
import {
	ScrollView,
	View,
	Text,
	Alert,
	Button,
	ActivityIndicator
} from "kony-ui";
import { Link } from "kony-router-ui";
import ResourceHeader from "../components/Resource/ResourceHeader";
import PropertiesList from "../components/UI/PropertiesList";
import InspectionModal from "../components/Modal/InspectionModal";
import TextHeader from "../components/UI/TextHeader";
import useFetch from "../hooks/useFetch";
import generalStyles from "../generalStyles";

const RentScreen = ({ userId, match }) => {
	const { rentId } = match.params,
		[isLoading, fetchError, fetchData] = useFetch(
			`https://car-rent-api.herokuapp.com/api/rents/${rentId}`
		),
		[rentState, setRentState] = useState(null),
		[showInspectionModal, setInspectionModal] = useState(false),
		toggleInspectionModal = () => setInspectionModal(prevState => !prevState),
		closeInspectionModal = ({ returnRent }) => {
			if (returnRent) {
				const currentDate = new Date()
					.toISOString()
					.slice(0, 19)
					.replace("T", " ");

				setRentState(prevState => ({
					rent: {
						...prevState.rent,
						returned_at: currentDate
					}
				}));
			}
			toggleInspectionModal();
		};

	useEffect(() => {
		if (fetchError) Alert.alert("Error", fetchError.message);
		else if (fetchData) setRentState(fetchData);
	}, [fetchError]);

	let content;

	if (rentState) {
		const {
				id,
				make,
				model,
				builtYear,
				frontImage,
				clientId,
				vehicleId,
				...rentData
			} = rentState.rent,
			screenTitle = `Rent #${id}`,
			screenSubTitle = `${make} ${model} ${builtYear}`;

		content = (
			<Fragment>
				<ResourceHeader title={screenTitle} imgUrl={frontImage} />
				<TextHeader fontSize={16}> {screenSubTitle} </TextHeader>
				<View>
					<Link to={`/vehicles/${vehicleId}`}>
						<Text style={generalStyles.linkText}> Vehicle Screen </Text>
					</Link>
				</View>
				<PropertiesList properties={rentData} />
				{userId === clientId && (
					<Button
						title="Return vehicle"
						disabled={!!rentData.returned_at}
						onPress={toggleInspectionModal}
					/>
				)}
				{rentState && (
					<InspectionModal
						showingModal={showInspectionModal}
						subTitle={screenSubTitle}
						frontImage={frontImage}
						rentId={rentId}
						employeeId={rentData.employee_id}
						closeHandler={closeInspectionModal}
					/>
				)}
			</Fragment>
		);
	}

	return (
		<ScrollView style={generalStyles.container}>
			{isLoading ? <ActivityIndicator /> : content}
		</ScrollView>
	);
};

export default RentScreen;
