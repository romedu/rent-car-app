import kony, { useContext } from "kony";
import { View, Text, StyleSheet } from "kony-ui";
import { Switch, Route, Redirect } from "kony-router-ui";
import ResourceScreen from "./ResourceScreen";
import RegisterScreen from "./RegisterScreen";
import LoginScreen from "./LoginScreen";
import VehicleScreen from "./VehicleScreen";
import RentScreen from "./RentScreen";
import InspectionScreen from "./InspectionScreen";
import ReportsScreen from "./ReportsScreen";
import Nav from "../components/Navigation/Nav";
import { AuthContext } from "../context/AuthContext";
import generalStyles from "../generalStyles";

const AppScreen = () => {
	const { userId, loginHandler, logoutHandler } = useContext(AuthContext);

	return (
		<View style={generalStyles.container}>
			<View style={styles.screenContainer}>
				<Switch>
					{userId && <Redirect from="/auth" to="/vehicles" />}
					{!userId && <Redirect from="/my-rents" to="/auth/login" />}
					{!userId && <Redirect from="/reports" to="/auth/login" />}
					<Route
						exact
						path="/vehicles"
						render={({ match }) => (
							<ResourceScreen
								title="Vehicles"
								match={match}
								url="https://car-rent-api.herokuapp.com/api/vehicles"
							/>
						)}
					/>
					<Route
						path="/vehicles/:vehicleId"
						render={({ match }) => (
							<VehicleScreen match={match} userId={userId} />
						)}
					/>
					<Route
						exact
						path="/rents"
						render={({ match }) => (
							<ResourceScreen
								title="Rents"
								match={match}
								url="https://car-rent-api.herokuapp.com/api/rents"
							/>
						)}
					/>
					<Route
						path="/rents/:rentId"
						render={({ match }) => (
							<RentScreen match={match} userId={userId} />
						)}
					/>
					<Route
						path="/my-rents"
						render={({ match }) => (
							<ResourceScreen
								title="My Rents"
								match={match}
								rootRoute="/rents"
								url="https://car-rent-api.herokuapp.com/api/rents"
								searchParams={`client_id=${userId}&rent.available=1`}
							/>
						)}
					/>
					<Route
						exact
						path="/inspections"
						render={({ match }) => (
							<ResourceScreen
								title="Inspections"
								match={match}
								url="https://car-rent-api.herokuapp.com/api/inspections"
							/>
						)}
					/>
					<Route
						path="/inspections/:inspectionId"
						render={({ match }) => <InspectionScreen match={match} />}
					/>
					<Route
						exact
						path="/auth/login"
						render={() => <LoginScreen loginHandler={loginHandler} />}
					/>
					<Route
						exact
						path="/auth/register"
						render={() => <RegisterScreen loginHandler={loginHandler} />}
					/>
					<Route
						exact
						path="/reports"
						render={() => <ReportsScreen userId={userId} />}
					/>
					<Route
						render={({ match }) => (
							<View>
								<Text> Not Found </Text>
							</View>
						)}
					/>
				</Switch>
			</View>
			<View>
				<Nav userId={userId} logoutHandler={logoutHandler} />
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	screenContainer: {
		flexGrow: 1,
		paddingBottom: 30
	}
});

export default AppScreen;
