import kony, { useState, useEffect, useContext } from "kony";
import {
	View,
	ActivityIndicator,
	StyleSheet,
	AsyncStorage
} from "kony-ui";
import TextHeader from "../components/UI/TextHeader";
import { AuthContext } from "../context/AuthContext";
import generalStyles from "../generalStyles";

const LandingScreen = ({ history }) => {
	const [isLoading, setLoading] = useState(true),
		{ loginHandler, logoutHandler } = useContext(AuthContext);

	// Controls when to leave the screen
	useEffect(() => {
		if (!isLoading) {
			history.push("/vehicles");
		}
	}, [isLoading, history]);

	// Validate the token
	useEffect(() => {
		const verifyToken = async () => {
			try {
				const token = await AsyncStorage.getItem("token"),
					tokenExp = await AsyncStorage.getItem("tokenExp"),
					currentTimeValue = new Date().valueOf();

				if (Number(tokenExp) > currentTimeValue) {
					const fetchOptions = {
							headers: { "Authorization": token }
						},
						fetchResult = await fetch(
							"https://car-rent-api.herokuapp.com/api/auth/renew-token",
							fetchOptions
						),
						{ error, token: newToken } = await fetchResult.json();

					if (!error) {
						loginHandler(newToken);
						setLoading(false);
					} else {
						throw new Error("Invalid/Expired Token");
					}
				} else {
					throw new Error("Invalid/Expired Token");
				}
			} catch (error) {
				logoutHandler();
				setLoading(false);
			}
		};

		setTimeout(verifyToken, 3000);
	}, []);

	return (
		<View style={[generalStyles.container, styles.landingContainer]}>
			<View style={styles.headerContainer}>
				<TextHeader color="white" textAlign="center" fontSize={28}>
					Rent Car App
				</TextHeader>
			</View>
			<ActivityIndicator size="large" />
		</View>
	);
};

const styles = StyleSheet.create({
	headerContainer: {
		marginBottom: 30
	},
	landingContainer: {
		backgroundColor: "black",
		flexDirection: "column",
		justifyContent: "center"
	}
});

export default LandingScreen;
