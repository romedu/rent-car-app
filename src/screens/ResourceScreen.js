import kony, { useState, useEffect } from "kony";
import { View, Alert, StyleSheet, ActivityIndicator } from "kony-ui";
import TextHeader from "../components/UI/TextHeader";
import ResourceList from "../components/Resource/ResourceList";
import generalStyles from "../generalStyles";

const ResourceScreen = ({
	title,
	url,
	rootRoute,
	match,
	searchParams = ""
}) => {
	const initialState = {
			resources: null,
			currentPage: 0,
			nextPage: true
		},
		[state, setState] = useState(initialState),
		getResources = async isContatenating => {
			try {
				if (!state.nextPage) return;

				const fetchUrl = `${url}?page=${state.currentPage +
						1}&${searchParams}`,
					fetchResults = await fetch(fetchUrl),
					parsedFetchData = await fetchResults.json();

				// Handle errors
				if (parsedFetchData.status && parsedFetchData.status !== 200) {
					throw new Error(parsedFetchData.message);
				}

				setState(prevState => ({
					resources: isContatenating
						? prevState.resources.concat(parsedFetchData.data)
						: parsedFetchData.data,
					currentPage: prevState.currentPage + 1,
					nextPage: parsedFetchData.data.length > 0
				}));
			} catch (error) {
				return Alert.alert("Error", error.message);
			}
		};

	useEffect(() => {
		setState(initialState);
	}, [match]);

	useEffect(() => {
		if (!state.resources) getResources();
	}, [state.resources]);

	return (
		<View style={[generalStyles.container, styles.screenContainer]}>
			<View style={styles.headerContainer}>
				<TextHeader fontSize={26} textAlign="center">
					{title}
				</TextHeader>
			</View>
			<View>
				{state.resources ? (
					<ResourceList
						match={match}
						rootRoute={rootRoute}
						resources={state.resources}
						getResources={getResources}
					/>
				) : (
					<View style={styles.loaderContainer}>
						<ActivityIndicator size="large" />
					</View>
				)}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	screenContainer: {
		paddingBottom: 40
	},
	headerContainer: {
		marginTop: 20,
		marginBottom: 15
	},
	loaderContainer: {
		paddingTop: 50
	}
});

export default ResourceScreen;
