import kony, { useContext } from "kony";
import { View, Text, Button, Alert } from "kony-ui";
import { Link } from "kony-router-ui";
import TextHeader from "../components/UI/TextHeader";
import TextInputField from "../components/UI/TextInputField";
import CheckInputField from "../components/UI/CheckInputField";
import useInputHandler from "../hooks/useInputHandler";
import generalStyles from "../generalStyles";

const RegisterScreen = ({ loginHandler }) => {
	const [inputState, inputChangeHandler] = useInputHandler({
			name: "",
			nationalId: "",
			username: "",
			password: "",
			passwordConfirm: "",
			cardNo: "",
			isJuridic: false
		}),
		submitHandler = async () => {
			try {
				const fetchOptions = {
						method: "POST",
						headers: { "Content-Type": "application/json" },
						body: JSON.stringify(inputState)
					},
					fetchResult = await fetch(
						"https://car-rent-api.herokuapp.com/api/auth/register",
						fetchOptions
					),
					{ error, token } = await fetchResult.json();

				if (error) throw new Error(error.message);
				else loginHandler(token);
			} catch (error) {
				Alert.alert("Error", error.message);
			}
		};

	return (
		<View style={generalStyles.container}>
			<View>
				<TextHeader> Register </TextHeader>
			</View>
			<View>
				<TextInputField
					name="name"
					placeholder="Name"
					label="Name"
					value={inputState.name}
					onChangeHandler={inputChangeHandler}
				/>
				<TextInputField
					name="nationalId"
					placeholder="National ID"
					label="National ID"
					value={inputState.nationalId}
					onChangeHandler={inputChangeHandler}
				/>
				<TextInputField
					name="cardNo"
					placeholder="Card No."
					label="Card Number"
					value={inputState.cardNo}
					onChangeHandler={inputChangeHandler}
				/>
				<TextInputField
					name="username"
					placeholder="Username"
					label="Username"
					value={inputState.username}
					onChangeHandler={inputChangeHandler}
				/>
				<TextInputField
					name="password"
					placeholder="Password"
					label="Password"
					value={inputState.password}
					onChangeHandler={inputChangeHandler}
					isPassword
				/>
				<TextInputField
					name="passwordConfirm"
					placeholder="Password Confirmation"
					label="Password Confirmation"
					value={inputState.passwordConfirm}
					onChangeHandler={inputChangeHandler}
					isPassword
				/>
				<CheckInputField
					name="isjuridic"
					label="Are you juridic?"
					value={inputState.isJuridic}
					onChangeHandler={inputChangeHandler}
				/>
			</View>
			<View>
				<Link to="/auth/login">
					<Text> Already an user? </Text>
				</Link>
			</View>
			<Button title="Submit" onPress={submitHandler} />
		</View>
	);
};

export default RegisterScreen;
