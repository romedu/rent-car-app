import kony, { useState, useEffect, Fragment } from "kony";
import { ScrollView, Alert, Button, ActivityIndicator } from "kony-ui";
import ResourceHeader from "../components/Resource/ResourceHeader";
import PropertiesList from "../components/UI/PropertiesList";
import RentConfModal from "../components/Modal/RentConfModal";
import useFetch from "../hooks/useFetch";
import generalStyles from "../generalStyles";

// In this screen data related the vehicle which id was passed in the match object will be displayed
// if the user is logged and the vehicle is avaible, a "Rent vehicle" button will be displayed below
const VehicleScreen = ({ userId, match }) => {
	const { vehicleId } = match.params,
		[isLoading, fetchError, fetchData] = useFetch(
			`https://car-rent-api.herokuapp.com/api/vehicles/${vehicleId}`
		),
		[vehicleState, setVehicleState] = useState(null),
		[showRentModal, setRentModal] = useState(false),
		toggleRentModal = () => setRentModal(prevState => !prevState),
		closeRentModal = ({ makeUnavailable }) => {
			if (makeUnavailable) {
				setVehicleState(prevState => ({
					vehicle: { ...prevState.vehicle, available: 0 }
				}));
			}
			toggleRentModal();
		};

	useEffect(() => {
		if (fetchError) Alert.alert("Error", fetchError.message);
		else if (fetchData) setVehicleState(fetchData);
	}, [fetchError, fetchData]);

	let content;

	if (vehicleState) {
		const {
				make,
				model,
				builtYear,
				frontImage,
				...vehicleData
			} = vehicleState.vehicle,
			screenTitle = `${make} ${model} ${builtYear}`;

		content = (
			<Fragment>
				<ResourceHeader title={screenTitle} imgUrl={frontImage} />
				<PropertiesList properties={vehicleData} />
				{userId && (
					<Button
						title="Rent vehicle"
						disabled={!vehicleData.available}
						onPress={toggleRentModal}
					/>
				)}
				{vehicleState && (
					<RentConfModal
						showingModal={showRentModal}
						make={make}
						model={model}
						builtYear={builtYear}
						frontImage={frontImage}
						closeHandler={closeRentModal}
						vehicleData={vehicleData}
					/>
				)}
			</Fragment>
		);
	}

	return (
		<ScrollView style={generalStyles.container}>
			{isLoading ? <ActivityIndicator /> : content}
		</ScrollView>
	);
};

export default VehicleScreen;
