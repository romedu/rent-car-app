import kony, { useState } from "kony";
import { View, Button, ActivityIndicator, Alert } from "kony-ui";
import TextHeader from "../components/UI/TextHeader";
import TextInputField from "../components/UI/TextInputField";
import useInputHandler from "../hooks/useInputHandler";
import generalStyles from "../generalStyles";

const ReportsScreen = ({ userId }) => {
	const [inputState, inputChangeHandler] = useInputHandler({ email: "" }),
		[isLoading, setLoader] = useState(false),
		sendReport = async reportType => {
			try {
				setLoader(true);

				const reportsUrl = {
						vehicle:
							"https://car-rent-api.herokuapp.com/api/vehicles/send-report",
						rent:
							"https://car-rent-api.herokuapp.com/api/rents/send-report",
						inspection:
							"https://car-rent-api.herokuapp.com/api/inspections/send-report",
						myRent: `https://car-rent-api.herokuapp.com/api/rents/send-report?client_id=${userId}&rent.available=1`
					},
					fetchOptions = {
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify(inputState)
					},
					fetchResult = await fetch(reportsUrl[reportType], fetchOptions),
					{ message, error } = await fetchResult.json();

				if (error) throw new Error(error.message);
				else Alert.alert("Message", message);
			} catch (error) {
				Alert.alert("Error", error.message);
			}

			setLoader(false);
		};

	return (
		<View style={generalStyles.container}>
			<TextHeader> Reports </TextHeader>
			<TextInputField
				label="Email to send the report to:"
				name="email"
				placeholder="Your email goes here"
				value={inputState.email}
				keyboardType="email"
				onChangeHandler={inputChangeHandler}
			/>
			<View style={{ marginTop: 15 }}>
				<Button
					title="Vehicles Report"
					disabled={isLoading}
					onPress={() => sendReport("vehicle")}
				/>
			</View>
			<View style={{ marginTop: 15 }}>
				<Button
					title="Rents Report"
					disabled={isLoading}
					onPress={() => sendReport("rent")}
				/>
			</View>
			<View style={{ marginTop: 15 }}>
				<Button
					title="Inspections Report"
					disabled={isLoading}
					onPress={() => sendReport("inspection")}
				/>
			</View>
			<View style={{ marginTop: 15 }}>
				<Button
					title="My Rents Report"
					disabled={isLoading}
					onPress={() => sendReport("myRent")}
				/>
			</View>
			<ActivityIndicator animating={isLoading} size="large" />
		</View>
	);
};

export default ReportsScreen;
