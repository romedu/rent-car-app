import kony, { useState } from "kony";
import { AsyncStorage } from "kony-ui";

const AuthContext = kony.createContext({
	currentUserId: null,
	token: null,
	tokenExp: null,
	loginHandler: () => {},
	logoutHandler: () => {}
});

const AuthContextProvider = ({ children }) => {
	const initialState = {
			userId: null,
			token: null,
			tokenExp: null
		},
		[state, setState] = useState(initialState),
		loginHandler = authData => {
			const { userId, token, tokenExp } = authData;

			// Aparently saving the tokenExp without waiting for this one to finish, kinds of like skips it
			AsyncStorage.setItem("token", token);
			AsyncStorage.setItem("tokenExp", JSON.stringify(tokenExp));
			setState({ userId, token, tokenExp });
		},
		logoutHandler = () => loginHandler(initialState);

	return (
		<AuthContext.Provider value={{ ...state, loginHandler, logoutHandler }}>
			{children}
		</AuthContext.Provider>
	);
};

export { AuthContext, AuthContextProvider };
