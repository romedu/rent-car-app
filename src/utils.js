// Converts a camel cased word to a phrase consising af all of its words capitalized and separated by a space
export const fromCamelToCapital = stringToConvert => {
	return stringToConvert[0].toUpperCase().concat(
		stringToConvert
			.substring(1)
			.split("")
			.map(char => (char === char.toUpperCase() ? ` ${char}` : char))
			.join("")
	);
};
