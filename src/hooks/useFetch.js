import { useState, useEffect } from "kony";

const useFetch = (fetchUrl, fetchOptions) => {
	const [isLoading, setLoader] = useState(true),
		[dataState, setDataState] = useState({
			error: null,
			fetchData: null
		});

	useEffect(() => {
		if (dataState.error || dataState.fetchData) {
			setLoader(false);
		}
	}, [dataState]);

	useEffect(() => {
		const getData = async () => {
			try {
				const fetchResult = await fetch(fetchUrl, fetchOptions),
					{ error, ...fetchData } = await fetchResult.json();

				if (error) {
					throw new Error(error.message);
				}

				setDataState({ fetchData });
			} catch (error) {
				setDataState({ error: error.message });
			}
		};

		getData();
	}, []);

	return [isLoading, dataState.error, dataState.fetchData];
};

export default useFetch;
