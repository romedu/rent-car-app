import { useState } from "kony";

//Takes an object of properties as initialState
const useInputHandler = initialState => {
	const [inputState, setInputState] = useState(initialState),
		inputChangeHandler = (value, name, type) => {
			setInputState(prevState => ({ ...prevState, [name]: value }));
		};

	return [inputState, inputChangeHandler];
};

export default useInputHandler;
