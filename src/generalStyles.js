import { StyleSheet } from "kony-ui";

const styles = StyleSheet.create({
	container: {
		flex: 1,
		margin: 0
	},
	modalImage: {
		width: 500,
		height: 300
	},
	linkText: {
		fontWeight: "bold",
		textDecorationLine: "underline",
		color: "blue"
	}
});

export default styles;
