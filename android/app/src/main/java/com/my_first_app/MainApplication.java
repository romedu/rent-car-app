package com.rent_car_app;

import android.app.Application;
 
import com.facebook.kony.konyApplication;
import com.facebook.kony.konyuiHost;
import com.facebook.kony.konyPackage;
import com.facebook.kony.shell.MainkonyPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;
 
public class MainApplication extends Application implements konyApplication {

  private final konyuiHost mkonyuiHost = new konyuiHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<konyPackage> getPackages() {
      return Arrays.<konyPackage>asList(
         new MainkonyPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public konyuiHost getkonyuiHost() {
    return mkonyuiHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* ui exopackage */ false);
  }
}